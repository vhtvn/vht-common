<?php

namespace Vht\Common;

class TelcoPrefixEnum extends Enum
{
    const MOBI_8490     = '8490';
    const MOBI_8493     = '8493';
    const MOBI_84120    = '84120';
    const MOBI_84126    = '84126';
    const MOBI_84121    = '84121';
    const MOBI_84122    = '84122';
    const MOBI_84128    = '84128';
    const MOBI_8489     = '8489';
    const MOBI_8470     = '8470';
    const MOBI_8476     = '8476';
    const MOBI_8477     = '8477';
    const MOBI_8478     = '8478';
    const MOBI_8479     = '8479';

    const VINA_8491     = '8491';
    const VINA_8494     = '8494';
    const VINA_84123    = '84123';
    const VINA_84124    = '84124';
    const VINA_84125    = '84125';
    const VINA_84127    = '84127';
    const VINA_84129    = '84129';
    const VINA_8488     = '8488';
    const VINA_8481     = '8481';
    const VINA_8482     = '8482';
    const VINA_8483     = '8483';
    const VINA_8484     = '8484';
    const VINA_8485     = '8485';

    const VIETTEL_8496  = '8496';
    const VIETTEL_8497  = '8497';
    const VIETTEL_8498  = '8498';
    const VIETTEL_84162 = '84162';
    const VIETTEL_84163 = '84163';
    const VIETTEL_84164 = '84164';
    const VIETTEL_84165 = '84165';
    const VIETTEL_84166 = '84166';
    const VIETTEL_84167 = '84167';
    const VIETTEL_84168 = '84168';
    const VIETTEL_84169 = '84169';
    const VIETTEL_8486  = '8486';
    const VIETTEL_8432  = '8432';
    const VIETTEL_8433  = '8433';
    const VIETTEL_8434  = '8434';
    const VIETTEL_8435  = '8435';
    const VIETTEL_8436  = '8436';
    const VIETTEL_8437  = '8437';
    const VIETTEL_8438  = '8438';
    const VIETTEL_8439  = '8439';

    const VNM_8492      = '8492';
    const VNM_84186     = '84186';
    const VNM_84188     = '84188';
    const VNM_8452      = '8452';
    const VNM_8456      = '8456';
    const VNM_8458      = '8458';

    const GMOBILE_8499  = '8499';
    const GMOBILE_84199 = '84199';
    const GMOBILE_8459  = '8459';

    const SFONE_8495    = '8495';
}
