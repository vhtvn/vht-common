<?php
namespace Vht\Common;

class Encryption
{
    public $publicKey = null;
    public $privateKey = null;

    /**
     * @param $publicKey    resource - a key, returned by openssl_get_publickey()
     * @param $privateKey   resource - a key, returned by openssl_get_privatekey()
     */
    public function __construct($publicKey, $privateKey)
    {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;
    }

    /**
     * @param $data
     *
     * @return string
     * @throws \Exception
     */
    public function encrypt($data)
    {
        if (openssl_public_encrypt($data, $encrypted, $this->publicKey))
            $data = base64_encode($encrypted);
        else
            throw new \Exception('Unable to encrypt data. Perhaps it is bigger than the key size?');

        return $data;
    }

    /**
     * @param $data
     *
     * @return string
     */
    public function decrypt($data)
    {
        if (openssl_private_decrypt(base64_decode($data), $decrypted, $this->privateKey))
            $data = $decrypted;
        else
            $data = '';

        return $data;
    }
    
    /**
     * Generate signature
     * @param $data
     *
     * @return string
     * @throws \Exception
     */
    public function generateSignRSA($data)
    {
        if (openssl_sign($data, $encrypted, $this->privateKey, OPENSSL_ALGO_SHA1))
            $data = base64_encode($encrypted);
        else
            throw new \Exception('Unable to generate sign data');
        openssl_free_key($this->privateKey);
        return $data;
    }

    /**
     * @param $data
     * @param $signature
     *
     * @return bool
     * @throws \Exception
     */
    public function verifySignRSA($data, $signature)
    {
        $pass = false;
        if (\openssl_verify($data, base64_decode($signature), $this->publicKey, OPENSSL_ALGO_SHA1))
            $pass = true;
        else
            throw new \Exception( openssl_error_string() );
        //openssl_free_key($this->publicKey);
        return $pass;
    }

    /**
     * @param $data
     * @param $signature
     *
     * @return bool
     * @throws \Exception
     */
    public function verifyHexSignRSA($data, $signature)
    {
        $pass = false;
        if (\openssl_verify($data, (pack('H*',$signature)), $this->publicKey, OPENSSL_ALGO_SHA1))
            $pass = true;
        else
            throw new \Exception( openssl_error_string() );
        //openssl_free_key($this->publicKey);
        return $pass;
    }

    /**
     * @param      $pathPrivate
     * @param      $pathPublic
     * @param null $passphrase
     */
    public function generateRSA($pathPrivate, $pathPublic, $passphrase = null)
    {
        // generate a 2048 bit rsa private key, returns a php resource, save to file
        $privateKey = openssl_pkey_new(array(
            'private_key_bits' => 1024,
            'private_key_type' => OPENSSL_KEYTYPE_RSA,
        ));
        openssl_pkey_export_to_file($privateKey, $pathPrivate, $passphrase);

        // get the public key $keyDetails['key'] from the private key;
        $keyDetails = openssl_pkey_get_details($privateKey);
        file_put_contents($pathPublic, $keyDetails['key']);
    }

    /**
     * @param $input
     * @param $key_seed
     *
     * @return string
     */
    public function tripleDESEncrypt($input, $key_seed)
    {
        $input = trim($input);
        $block = mcrypt_get_block_size('tripledes', 'ecb');
        $len = strlen($input);
        $padding = $block - ($len % $block);
        $input .= str_repeat(chr($padding),$padding);
        // generate a 24 byte key from the md5 of the seed
        $key = substr(md5($key_seed),0,24);
        $iv_size = mcrypt_get_iv_size(MCRYPT_TRIPLEDES, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        // encrypt
        $encrypted_data = mcrypt_encrypt(MCRYPT_TRIPLEDES, $key, $input,
            MCRYPT_MODE_ECB, $iv);
        // clean up output and return base64 encoded
        return base64_encode($encrypted_data);
    }

    /**
     * @param $input
     * @param $key_seed
     *
     * @return string
     */
    public function tripleDESDecrypt($input, $key_seed)
    {
        $input = base64_decode($input);
        $key = substr(md5($key_seed),0,24);
        $text=mcrypt_decrypt(MCRYPT_TRIPLEDES, $key, $input, MCRYPT_MODE_ECB,'12345678');
        $block = mcrypt_get_block_size('tripledes', 'ecb');
        $packing = ord($text{strlen($text) - 1});
        if($packing and ($packing < $block)) {
            for($P = strlen($text) - 1; $P >= strlen($text) - $packing; $P--) {
                if(ord($text{$P}) != $packing) {
                    $packing = 0;
                }
            }
        }
        $text = substr($text,0,strlen($text) - $packing);
        return $text;
    }

    /**
     * @param int $length
     *
     * @return string
     */
    public function generateRandomString($length = 30)
    {
        $characters='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0987654321'.time();
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = $length; $i > 0; $i--)
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param $string
     *
     * @return string
     */
    public static function strToHex($string)
    {
        $hex = '';
        for ($i=0; $i<strlen($string); $i++){
            $ord = ord($string[$i]);
            $hexCode = dechex($ord);
            $hex .= substr('0'.$hexCode, -2);
        }
        return strToUpper($hex);
    }

    /**
     * @param $hex
     *
     * @return string
     */
    public static function hexToStr($hex)
    {
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2){
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $string;
    }
}