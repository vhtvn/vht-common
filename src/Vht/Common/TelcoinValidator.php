<?php
namespace Vht\Common;

/**
 * Validate a Telco identification number (TELCOIN)
 *
 */
class TelcoinValidator
{
    /**
     * @var
     */
    public $prefix = '';

    /**
     * Regular expression patterns per telco
     *
     * @var array
     */
    protected $patterns = array(
        '849' => '\d{8}',
        '841' => '\d{9}',
        '848' => '\d{8}',
        '845' => '\d{8}',
        '843' => '\d{8}',
        '847' => '\d{8}',
    );

    /**
     * Returns true if value is a valid Telco identification number, false otherwise
     * @param string    $value          Value
     *
     * @return bool
     */
    public function isValid($value)
    {
        if (null === $value || '' === $value) {
            return false;
        }
        $countryCode = substr($value, 0, 3);
        $tin = substr($value, 3);
        if (false === $this->isValidCountryCode($countryCode)) {
            return false;
        }
        if (0 === preg_match('/^'.$this->patterns[$countryCode].'$/', $tin)) {
            return false;
        } else {
            $this->prefix = strlen($value) == 11 ? substr($value, 0, 4) : substr($value, 0, 5);
        }

        return true;
    }

    /**
     * Returns true if value is valid telco code, false otherwise
     *
     * @param string $value Value
     *
     * @return bool
     */
    public function isValidCountryCode($value)
    {
        return isset($this->patterns[$value]);
    }
}